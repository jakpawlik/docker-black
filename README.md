# Black Sulu Docker Install

Instalacja czarnej wersji sulu przez Dockera

Docker tworzy maszynę wirtualną na komputerze hosta (w przykładzie Windows) z kontenerami (podmaszynami) które się ze sobą komunikują. 
Każdy kontener może mieć inny system szczegóły w paczkach w docker-compose.yml. Trzeba szukać w katalogu: (https://hub.docker.com)

**My dyskutujemy przez port 80 z kontenerem nginx** (nazwa: black-docker-nginx)

## Wymagania

* [VirtualBox](https://www.virtualbox.org)
* [Docker Tools for Windows](https://docs.docker.com/toolbox/toolbox_install_windows/)
* [Docker Compose for Windows](https://docs.docker.com/compose/)
* Folder **VirtualBox** ma być dodany do zmiennej środowiskowej **PATH**

## Zasoby serwera instalowanego przez dockera

* NGINX
* PHP-FPM
* MySQL
* Elasticsearch
* GIT/SSH

## Instalacja

Zakładamy że folder projektu to: *D:/www/abc*

1. *Ustaw folder nadrzędny projektu i ine dane w pliku .env (skopiuj z .env.dist)*
2. *Ustaw konfigurację stron w ./phpdocker/nginx/config.yaml (skopiuj z config.yaml.dist)*
3. Dodaj do docker-compose ścieżki do vendors jeśli używasz symfony. Podobnie z folderami cache.
4. Uruchamiamy Docker Quickstart Terminal - wyświetli ip. dodajemy je do pliku hosts na domeny ustawione w pliku config.yaml
5. Wchodzimy w folder głowny repo i wyłączamy maszynę:
```bash
	docker-machine stop
```
6. Przy wyłączonej maszynie wirtualnej wprowadzamy komendę (Dla swojego dysku):
```bash
	vboxmanage sharedfolder add default --name "drive" --hostpath "D:/" --automount
```
7. od teraz D: to /drive we wszystkich kontenerach - /application odpowiada folderowi /code z repo startera
8. Uruchom maszynę
```bash
	docker-machine start
```
9. Po odpaleniu się maszyny uruchom skrypt startowy:
```bash 
	./start.sh
```
10. Do kontenera nginx dostajemy się poleceniem
```bash 
	./ssh.sh
```
11. Do kontenera php dostajemy się poleceniem
```bash 
	./ssh.sh php
```
12. Port bazy wewnątrz dockera odpowiada zmiennej z .env DB_PORT ale port na zewnątrz dockera ustawiany jest na wartość HOST_DB_PORT
13. Podobnie port HTTP serwera nginx