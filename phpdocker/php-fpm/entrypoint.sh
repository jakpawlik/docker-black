#!/bin/bash

service php7.3-fpm start
cd /var && npm install --unsafe-perm
node /var/envinstall.js /var/.src.env.local /application .env.local


# Ah, ha, ha, ha, stayin' alive...
while :; do :; done & kill -STOP $! && wait $!