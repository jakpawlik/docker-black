#!/bin/node

fs = require('fs');
var shell = require('shelljs');

var myArgs = process.argv.slice(2);
const YAML = require('yaml');

function exec(command, name){

	var code = shell.exec(command).code;

	if(name && code === 0){
		return;
	}else if(!name && code !== 0){
		name = 'command "' + command + '"';
	}

	return shell.echo((code === 0 ? '' : 'Error: ') + name + ' ' + (code === 0 ? 'succeeded' :  'failed') + '.');
}

String.prototype.allReplace = function(obj) {
    var retStr = this;

    for (var x in obj) {
        retStr = retStr.replace(new RegExp(x, 'g'), obj[x]);
    }

    return retStr;
};


const config = fs.readFileSync('/var/config/config.yaml', 'utf8')
let configParsed = YAML.parse(config);

const servers = configParsed.docker_config.servers;

let toReplace = fs.readFileSync(myArgs[0], 'utf8');

let output = '';

let serverIterator = 0;

Object.keys(servers).forEach( key => {
	let replacements = {}
	let val = configParsed.docker_config.servers[key];

	replacements['\\${siteFolder}'] = key;

	if(serverIterator === 0){
		replacements['\\${default}'] = ' default';
	}else{
		replacements['\\${default}'] = '';
	}

	Object.keys(process.env).forEach( key => {
		replacements['\\${' + key + '}'] = process.env[key];
	});

	Object.keys(val).forEach(keyVar => {
		let valVar = val[keyVar];

		replacements['\\${' + keyVar + '}'] = valVar;
	});	

	console.log(replacements);

	let replaced = toReplace.allReplace(replacements);

	output += replaced;
	serverIterator++;
});


let currentDir = require('path').dirname(require.main.filename);



var toFile = myArgs[1] + '/' + myArgs[2];

fs.writeFile( toFile, output, (err) => {
  	if (err) throw err;
  	console.log('Project configuration updated!');
});