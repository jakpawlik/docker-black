#!/bin/sh

npm install --unsafe-perm
node /var/envinstall.js /var/.src.app.conf /etc/nginx/conf.d app.conf
nginx

# Ah, ha, ha, ha, stayin' alive...
while :; do :; done & kill -STOP $! && wait $!